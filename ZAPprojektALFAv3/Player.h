#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QGraphicsRectItem>
#include <QKeyEvent>

#include "myScene.h"

class Player:
        public QObject,
        public QGraphicsRectItem
{
    Q_OBJECT

    unsigned short area_begining_delimeter;
    unsigned short x_area_ending_delimeter, y_area_ending_delimeter;

    myScene *curr_scene;

public:
    Player(myScene *);

    void keyPressEvent(QKeyEvent *event);

    void set_moving_area();
    void set_curr_scene(myScene *);

    myScene *get_curr_scene();

signals:
    void action();
};

#endif // PLAYER_H
