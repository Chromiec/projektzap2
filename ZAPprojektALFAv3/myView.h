#ifndef MYVIEW_H
#define MYVIEW_H

#include <QGraphicsView>

class myView:
        public QGraphicsView
{
    Q_OBJECT

public:
    myView();

public slots:
    void change_view(unsigned short);
};

#endif // MYVIEW_H
