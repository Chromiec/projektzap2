#include "myScene.h"

myScene::myScene(unsigned short scene_Width, unsigned short scene_Height):
    min_scene_Width(800),//jakieś małe ale na tyle by pokój razem ze ścianą nie był za ciasny, np 300 dla korytarza
    min_scene_Height(600),//ale tak by sie gracz jeszcze zmieścił
    max_scene_Width(65500),//jak na razie po prostu maksymalna wartość dla unsigned short 65535 chociaż
    max_scene_Height(65500),//i tak nie da się podać samemu czegoś większego niż właśnie koniec przedziału bo agrumenty konstruktora tego nie przyjmą
    wall_Size(100),
    floor(new QGraphicsRectItem),//trzeba najpierw stworzyć żeby użyć na niej komendy setRect() ale nie można jeśli przypisało się NULL (wywala program)
    scene_up(NULL),
    scene_right(NULL),
    scene_down(NULL),
    scene_left(NULL),
    door_up(NULL),//wstępne utworzenie tych drzwi chociażby jako null wydaje się być kluczowe dla działania mechanizmu ponownego łączenia gracza z
    door_right(NULL),//drzwiami jego sceny na którą właśnie przeszedł w myView::change_view(), drzwi muszą być jakoś zainicjowane jeśli warunek
    door_down(NULL),//ma być poprawnie sprawdzony
    door_left(NULL)
{
    if(scene_Width < min_scene_Width) scene_Width = min_scene_Width;//korekta podanych wartości
    if(scene_Height < min_scene_Height) scene_Height = min_scene_Height;
    if(scene_Width > max_scene_Width) scene_Width = max_scene_Width;
    if(scene_Height > max_scene_Height) scene_Height = max_scene_Height;

    set_mySceneRect(scene_Width, scene_Height);//ustawianie romzmiarów sceny
}

void myScene::set_mySceneRect(unsigned short scene_Width, unsigned short scene_Height)
{
    setSceneRect(0, 0, scene_Width, scene_Height);
    floor->setRect(0, 0, scene_Width-(2*wall_Size), scene_Height-(2*wall_Size));//ustawianie rozmiaru podłogi w zależności od rozmiaru pokoju
    floor->setPos(wall_Size, wall_Size);//ustawianie podłogi w odpowiednim miejscu
    addItem(floor);
}

void myScene::Floor(bool set)
{
    if(set && !floor)
    {
        addItem(floor);
    }
    else if(!set && floor)
    {
        removeItem(floor);
        floor = NULL;
    }
}

void myScene::set_scene_up(myScene *scene_to_set)
{
    scene_up = scene_to_set;
    door_up = new Door(0);
    door_up->setPos((width()/2)-(door_up->rect().width()/2), 0);
    addItem(door_up);
}

void myScene::set_scene_right(myScene *scene_to_set)
{
    scene_right = scene_to_set;
    door_right = new Door(1);
    door_right->setPos(width()-door_right->rect().width(), (height()/2)-(door_right->rect().height()/2));
    addItem(door_right);
}

void myScene::set_scene_down(myScene *scene_to_set)
{
    scene_down = scene_to_set;
    door_down = new Door(2);
    door_down->setPos((width()/2)-(door_down->rect().width()/2), height()-door_down->rect().height());
    addItem(door_down);
}

void myScene::set_scene_left(myScene *scene_to_set)
{
    scene_left = scene_to_set;
    door_left = new Door(3);
    door_left->setPos(0, (height()/2)-(door_left->rect().height()/2));
    addItem(door_left);
}

unsigned short myScene::get_wall_Size(){return wall_Size;}
QGraphicsRectItem *myScene::get_floor(){return floor;}
myScene *myScene::get_scene_up(){return scene_up;}
myScene *myScene::get_scene_right(){return scene_right;}
myScene *myScene::get_scene_down(){return scene_down;}
myScene *myScene::get_scene_left(){return scene_left;}
Door *myScene::get_door_up(){return door_up;}
Door *myScene::get_door_right(){return door_right;}
Door *myScene::get_door_down(){return door_down;}
Door *myScene::get_door_left(){return door_left;}
