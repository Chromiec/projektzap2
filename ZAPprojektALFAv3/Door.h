#ifndef DOOR_H
#define DOOR_H

#include <QObject>
#include <QGraphicsRectItem>

class Door:
        public QObject,
        public QGraphicsRectItem
{
    Q_OBJECT

    const unsigned short door_id;

public:
    Door(unsigned short);

signals:
    void door_used(unsigned short);

public slots:
    void reaction();
};

#endif // DOOR_H
