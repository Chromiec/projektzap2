#include "Door.h"

#include "Player.h"
#include "myView.h"

extern Player *player;
extern myView *view;

Door::Door(unsigned short give_id):
    door_id(give_id)
{
    switch(door_id)
    {
        case 0://door up
        {
            setRect(0, 0, 50, 100);
        }
            break;
        case 1://door right
        {
            setRect(0, 0, 100, 50);
        }
            break;
        case 2://door down
        {
            setRect(0, 0, 50, 100);
        }
            break;
        case 3://door left
        {
            setRect(0, 0, 100, 50);
        }
            break;
    }
    connect(this, &Door::door_used, view, &myView::change_view);//tylko po to jest tu extern view
}

void Door::reaction()
{
    switch(door_id)
    {
        case 0://door up
        {
            if(pos().y()+player->pos().y() < rect().height()+30)//warunek w osi y
                if(pos().x()-player->pos().x()-player->rect().width() <= 0)//1 warunek w osi x
                    if(pos().x()-player->pos().x() >= -rect().width())//2 warunek w osi x
                    {
                        emit door_used(door_id);
                    }
        }
            break;
        case 1://door rigth
        {
            if(pos().x()-player->pos().x() < player->rect().width()+30)
                if(pos().y()-player->pos().y()-player->rect().height() <= 0)
                    if(pos().y()-player->pos().y() >= -rect().height())
                    {
                        emit door_used(door_id);
                    }
        }
            break;
        case 2://door down
        {
            if(pos().y()-player->pos().y() < player->rect().height()+30)
                if(pos().x()-player->pos().x()-player->rect().width() <= 0)
                    if(pos().x()-player->pos().x() >= -rect().width())
                    {
                        emit door_used(door_id);
                    }
        }
            break;
        case 3://door left
        {
            if(pos().x()+player->pos().x() < rect().width()+30)
                if(pos().y()-player->pos().y()-player->rect().height() <= 0)
                    if(pos().y()-player->pos().y() >= -rect().height())
                    {
                        emit door_used(door_id);
                    }
        }
            break;
    }
}
