#include "myView.h"

#include "Player.h"

extern Player *player;

myView::myView()
{
    show();//to chyba wyciągne później do klasy Game
    //tu będzie setFixedSize() w zależności od tego jak ustalimy co z rozdzielczością
    setFixedSize(800, 600);//tymczasowo
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void myView::change_view(unsigned short door_id)
{
    if(player->get_curr_scene()->get_door_up())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_up(), &Door::reaction);
    if(player->get_curr_scene()->get_door_right())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_right(), &Door::reaction);
    if(player->get_curr_scene()->get_door_down())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_down(), &Door::reaction);
    if(player->get_curr_scene()->get_door_left())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_left(), &Door::reaction);
    scene()->removeItem(player);
    switch(door_id)
    {
        case 0:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_up());
            setScene(player->get_curr_scene());
        }
            break;
        case 1:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_right());
            setScene(player->get_curr_scene());
        }
            break;
        case 2:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_down());
            setScene(player->get_curr_scene());
        }
            break;
        case 3:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_left());
            setScene(player->get_curr_scene());
        }
    }
    player->set_moving_area();
    player->setFocus();
    if(player->get_curr_scene()->get_door_up())
        connect(player, &Player::action, player->get_curr_scene()->get_door_up(), &Door::reaction);
    if(player->get_curr_scene()->get_door_right())
        connect(player, &Player::action, player->get_curr_scene()->get_door_right(), &Door::reaction);
    if(player->get_curr_scene()->get_door_down())
        connect(player, &Player::action, player->get_curr_scene()->get_door_down(), &Door::reaction);
    if(player->get_curr_scene()->get_door_left())
        connect(player, &Player::action, player->get_curr_scene()->get_door_left(), &Door::reaction);
    scene()->addItem(player);
}
