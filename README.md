Aby przetestować obecną wersję programu wystarczy uruchomić ją z poziomu Qt
i nie używać myszki gdy okno programu jest aktywne, wynika to z błędu który
po użyciu myszki odbiera użytkownikowi możliwość sterowania.

W obecnej wersji programu jest generowana losowo mapa, stare błędy powodujące
okazjonalne wysypywanie się programu zostały poprawione i już nie powinny 
wystąpić, jeśli jednak wystąpią, można spróbować uruchomić program pare razy
a za którymś powinno być w porządku.
Z racji poprawek, kod w module odpowiedzialnym za generowanie jest w formacie
który był wykorzystywany do testów, więc zostały wprowadzone zmienne pomocnicze,
zmienne odpowiedzialne za parametry liczbowe mają przesadnie duże wartości
które pozwalają na łatwe wyłapanie błędu a funkcje mają podstawowe instrukcje.