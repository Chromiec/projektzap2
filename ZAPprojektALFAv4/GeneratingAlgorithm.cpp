#include "GeneratingAlgorithm.h"

#include <stdlib.h>
#include <time.h>
#include <QDebug>
SceneSeries::SceneSeries():
    scene_row(0),
    scene_column(0),
    created_scene(new myScene)
{}//tu w przyszłości może być mały algorytm losujący rozmiar sceny itp

GenerateMap::GenerateMap():
    max_corridors_in_map(10),
    corridors_counter(0),
    max_scenes_in_corridor(4),
    scenes_in_corridor_counter(0),
    N(9),
    map(new bool *[N]),
    head(new SceneSeries),
    tail(new SceneSeries),
    temporary(new SceneSeries[max_scenes_in_corridor])
{
    for(short i=0; i<N; i++)
        map[i] = new bool [N];
    for(short i=0; i<N; i++)
        for(short j=0; j<N; j++)
            map[i][j] = false;
    srand(time(NULL));
}

void GenerateMap::StartGenerating()
{
    short starting_row = 8;//N-1
    short starting_column = 4;//N/2
    head->scene_row = starting_row;//head będzie w takiej pozycji że niemożliwe będzie utworzenie pokoju pod nim, więc nie trzeba się martwić o to czy scena
    //startowa będzie ustawiona przed wygenerowaniem mapy, ale jak ktoś bardzo chce to zawsze można przkazać taką scenę do tej funkcji i przypisać
    //ją jeszcze przed generacją
    head->scene_column = starting_column;//te współrzędne są określone tak by skojarzyć położenie sceny z head zgodnie z
    map[starting_row][starting_column] = true;//informacją o położeniu tej sceny na orientacyjnej mapie

    GenerateCorridor(head);

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //GenerateCorridor(temporary[0]);//to też jest błędnym zapisem
    SceneSeries *possibilites;
    SceneSeries *second_chance_possibilities;
    short second_chances = 0;
    short starting_range = max_scenes_in_corridor; //(max_scenes_in_corridor*corridors_counter)-(corridors_counter-1);
    while(corridors_counter < max_corridors_in_map)
    {
        if(corridors_counter == 1)//jeśli do tej pory użycie GenerateCorridor() odbyło się tylko na head
        {
            bool *block = new bool[starting_range];//tablica do sprawdzania która z zapamiętanych scen może generować dalsze korytarze
            for(short i=0; i<starting_range; i++)//początkowe ustawienie tablicy sprawdzającej
                block[i] = false;
            short range = starting_range;//początkowy zakres teblicy
            determinate_possible_generation_range(starting_range, block, range);
            if(range > 0)
            {
                possibilites = new SceneSeries[range];//pojemnik na możliwości losowania z konkretnych scen
                for(short i=0, j=0; i<starting_range; i++)
                    if(!block[i])
                    {
                        possibilites[j].scene_row = temporary[i].scene_row;
                        possibilites[j].scene_column = temporary[i].scene_column;
                        possibilites[j].created_scene = temporary[i].created_scene;
                        j++;
                    }
                delete [] block;
                SceneSeries *to_add = new SceneSeries;
                short ran = rand()%range;
                to_add->scene_row = possibilites[ran].scene_row;//wybranie sceny z której wylosuje się nowy korytarz
                to_add->scene_column = possibilites[ran].scene_column;
                to_add->created_scene = possibilites[ran].created_scene;
                if(range > 1)//jeśli było więcej możliwości do wyboru, to przenieś je do kolejnej iteracji
                {
                    second_chances = range-1;
                    second_chance_possibilities = new SceneSeries[second_chances];
                    for(short i=0, j=0; i<range; i++)
                        if(i != ran)
                        {
                            second_chance_possibilities[j].scene_row = possibilites[i].scene_row;
                            second_chance_possibilities[j].scene_column = possibilites[i].scene_column;
                            second_chance_possibilities[j].created_scene = possibilites[i].created_scene;
                            j++;
                        }
                }
                delete [] possibilites;
                GenerateCorridor(to_add);
            }
            else
                corridors_counter++;
        }
        else
        {
            bool *block = new bool[starting_range];
            for(short i=0; i<starting_range; i++)
                block[i] = false;
            short range = starting_range;
            determinate_possible_generation_range(starting_range, block, range);
            if(range > 0)
            {
                possibilites = new SceneSeries[range];
                for(short i=0, j=0; i<starting_range; i++)
                    if(!block[i])
                    {
                        possibilites[j].scene_row = temporary[i].scene_row;
                        possibilites[j].scene_column = temporary[i].scene_column;
                        possibilites[j].created_scene = temporary[i].created_scene;
                        j++;
                    }
                delete [] block;
                SceneSeries *to_add = new SceneSeries;
                if(second_chances > 0)
                {
                    SceneSeries *temp = new SceneSeries[range];
                    for(short i=0; i<range; i++)
                    {
                        temp[i].scene_row = possibilites[i].scene_row;
                        temp[i].scene_column = possibilites[i].scene_column;
                        temp[i].created_scene = possibilites[i].created_scene;
                    }
                    range += second_chances;
                    delete [] possibilites;
                    possibilites = new SceneSeries[range];
                    for(short i=0; i<range; i++)
                    {
                        if(i<range-second_chances)
                        {
                            possibilites[i].scene_row = temp[i].scene_row;
                            possibilites[i].scene_column = temp[i].scene_column;
                            possibilites[i].created_scene = temp[i].created_scene;
                        }
                        else
                        {
                            possibilites[i].scene_row = second_chance_possibilities[i-(range-second_chances)].scene_row;
                            possibilites[i].scene_column = second_chance_possibilities[i-(range-second_chances)].scene_column;
                            possibilites[i].created_scene = second_chance_possibilities[i-(range-second_chances)].created_scene;
                        }
                    }
                }
                delete [] second_chance_possibilities;
                short ran = rand()%range;
                to_add->scene_row = possibilites[ran].scene_row;
                to_add->scene_column = possibilites[ran].scene_column;
                to_add->created_scene = possibilites[ran].created_scene;
                if(range > 1)
                {
                    second_chances = range-1;
                    second_chance_possibilities = new SceneSeries[second_chances];
                    for(short i=0, j=0; i<range; i++)
                        if(i != ran)
                        {
                            second_chance_possibilities[j].scene_row = possibilites[i].scene_row;
                            second_chance_possibilities[j].scene_column = possibilites[i].scene_column;
                            second_chance_possibilities[j].created_scene = possibilites[i].created_scene;
                            j++;
                        }
                }
                delete [] possibilites;
                GenerateCorridor(to_add);
            }
            else
                corridors_counter++;
        }
    }
}

void GenerateMap::GenerateCorridor(SceneSeries *current)
{
    bool block[] = {false, false, false, false};

    if(current->scene_row == 0 || current->created_scene->get_scene_up())           block[0] = true;
    if(current->scene_column == N-1 || current->created_scene->get_scene_right())   block[1] = true;
    if(current->scene_row == N-1 || current->created_scene->get_scene_down())       block[2] = true;
    if(current->scene_column == 0 || current->created_scene->get_scene_left())      block[3] = true;

    if(current->scene_row > 0 && current->scene_row < N-1)
    {
        if(map[current->scene_row-1][current->scene_column])    block[0] = true;
        if(map[current->scene_row+1][current->scene_column])    block[2] = true;
    }
    if(current->scene_column > 0 && current->scene_column < N-1)
    {
        if(map[current->scene_row][current->scene_column+1])    block[1] = true;
        if(map[current->scene_row][current->scene_column-1])    block[3] = true;
    }

    //w tych warunkach jest sprawdzane czy sceny z tych pozycji mają swoich fizycznych sasiadów (w postaci scene_up itd)
    //ale jest to tylko dodatkowe zabezpieczenie bo w trakcie tworzenia scen i tak jest określane ich położenie na mapie dzięki któremu
    //te warunki i tak by wiedziały o tym gdzie mogą jeszcze stworzyć scenę a gdzie nie, jedyny sens tego może być wtedy gdy ktoś
    //podaje scenę startową i chce mieć pewność że head nie utworzy sceny pod sobą gdzie ma się znajdować z założenia projektu scena startowa
    //to że to się nie sypie to właśnie kwestia dobrze ułożonych warunków postępowania w tym algorytmie dla dobranych założeń jego działania

    if(!(block[0] && block[1] && block[2] && block[3]))//to tylko dodatkowe zabezpieczenie, w praktyce nie jest możliwe by została podana scena
    {//która nie spełniłaby tego warunku
        SceneSeries *generated = new SceneSeries();//stwórz nową scenę

        short range = 4;//początkowy zakres możliwośći co do wyboru kierunku generowania
        for(short i=0; i<4; i++)//sprawdzenie aktualnej ilości możliwośći
            if(block[i])//jeśli jest informacja o zablokowaniu kierunku to zmniejsz zakres
                range--;

        short possibilites[range];//pojemnik na możliwośći losowania kierunków
        for(short i=0, j=0; i<4; i++)
            if(!block[i])//ten kierunek który nie jest zablokowany zostanie zapisany jako informacja do tablicy z której zostanie wylosowany jakiś kierunek
            {
                possibilites[j] = i;//wpisanie możliwych do wylosowania kierunków
                j++;//jeśli wpisano to iteruj przejście po tablicy do wpisywania
            }

        short ran = rand()%range;//losowanie z zakresu ustalonego przez ilość możliwości
        short choice = possibilites[ran];//wybranie jednej z możlowości
        qDebug()<<"choice"<<choice<<'\t'<<current->scene_row<<'\t'<<current->scene_column;
        if(choice == 0)
        {
            generated->scene_row = current->scene_row-1;//określ współrzędne kolejnej sceny
            generated->scene_column = current->scene_column;
            current->created_scene->set_scene_up(generated->created_scene);//połącz sceny
            generated->created_scene->set_scene_down(current->created_scene);
        }
        if(choice == 1)
        {
            generated->scene_row = current->scene_row;
            generated->scene_column = current->scene_column+1;
            current->created_scene->set_scene_right(generated->created_scene);
            generated->created_scene->set_scene_left(current->created_scene);
        }
        if(choice == 2)
        {
            generated->scene_row = current->scene_row+1;
            generated->scene_column = current->scene_column;
            current->created_scene->set_scene_down(generated->created_scene);
            generated->created_scene->set_scene_up(current->created_scene);
        }
        if(choice == 3)
        {
            generated->scene_row = current->scene_row;
            generated->scene_column = current->scene_column-1;
            current->created_scene->set_scene_left(generated->created_scene);
            generated->created_scene->set_scene_right(current->created_scene);
        }
        qDebug()<<generated->scene_row<<'\t'<<generated->scene_column;
        if(generated->scene_row<0 || generated->scene_row>N-1 || generated->scene_column<0 || generated->scene_column>N-1)
            return;//qDebug()<<generated->scene_row<<'\t'<<generated->scene_column;
        else
            map[generated->scene_row][generated->scene_column] = true;//zaznacz na mapie obecność nowej sceny

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //temporary[scenes_in_corridor_counter] = current;//nie wiem czemu ale ten zapis nie działa
        temporary[scenes_in_corridor_counter].scene_row = current->scene_row;//zapamiętaj pozycję sceny do ponownego losowania lub zapamiętaj całą scenę,
        //jeśli mam zapamiętać tylko współrzędne to potem musiałbym przeszukać listę w poszukiwaniu zgodnej sceny
        temporary[scenes_in_corridor_counter].scene_column = current->scene_column;
        temporary[scenes_in_corridor_counter].created_scene = current->created_scene;
        scenes_in_corridor_counter++;
        qDebug()<<current->scene_row<<'\t'<<current->scene_column;

        //qDebug()<<"koniec"<<r<<'\t'<<c;
        if(scenes_in_corridor_counter < max_scenes_in_corridor)//dzięki takiemu warunku w przy tworzeniu ostatniej sceny ten if się nie wykona,
        {
            //qDebug()<<"koniec"<<r<<'\t'<<c;
            GenerateCorridor(generated);//więc ostatnia scena nie będzie wpisana do tablicy temp, ponieważ tam wpisuje się
            }//aktualnie obecna scena dla tej funkcji a nie następna
        //przy tej rekurencji coś działa nie tak,  czasem, szczególnie dla większej liczby powtórzeń, zmienne odpowiedzialne za informację o współrzednych
        //przyjmują bardzo dziwne wartości chociaż nie są zmieniane w programie, np do czasu zakończenia tej funkcji mogą mieć normalną wartość (1, 5)
        //a w trakcie wykonywania kolejnej mają wartości zazwyczaj w okolicach (-4083, -17747)
        //pierwszym pomysłem na naprawienie tego jest pozbycie się rekurencji, drugim to usunięcie struct SceneSeries i przeniesienie zmiennych
        //przechowujących współrzędny do klasy myScene

        //okazało się że wystarczy zainicjalizować wstępnie zmienne scene_row oraz scene_column
        else
        {
            corridors_counter++;//kiedy jest to już ostatni pokój, zliczy się kolejny korytarz a licznik scen w korytarzu się zresetuje
            qDebug()<<"korytarz "<<corridors_counter;
            scenes_in_corridor_counter = 0;//dzięki swojemu położeniu nie będzie zawadzał z warunkiem który wywołuje generowanie kolejnych scen
            tail = current;//zapamiętaj ostatni wygenerowany
        }
    }
    else
    {
        corridors_counter++;
        scenes_in_corridor_counter = 0;
        tail = current;
    }
}

void GenerateMap::determinate_possible_generation_range(short starting_range, bool *block, short &range)
{
    for(short i=0; i<starting_range; i++)
    {
        //jeżeli dana scena nie będzie miała możliwości do dalszego losowania to trzeba ją wykluczyć z wyboru
        if(temporary[i].scene_row == 0)//sprawdzanie pozycji skrajnej wertykalnej górnej
        {
            if(temporary[i].scene_column == 0)//sprawdzanie pozycji lewy górny róg
            {
                if(map[temporary[i].scene_row][temporary[i].scene_column+1])
                    if(map[temporary[i].scene_row+1][temporary[i].scene_column])
                    {
                        block[i] = true;
                        range--;
                    }
            }
            else if(temporary[i].scene_column == N-1)//prawy górny róg
            {
                if(map[temporary[i].scene_row+1][temporary[i].scene_column])
                    if(map[temporary[i].scene_row][temporary[i].scene_column-1])
                    {
                        block[i] = true;
                        range--;
                    }
            }
            else//sprawdzanie pozycji skrajnej wertykalnej kiedy brak skrajnej horyzontalnej
            {
                if(map[temporary[i].scene_row][temporary[i].scene_column+1])
                    if(map[temporary[i].scene_row+1][temporary[i].scene_column])
                        if(map[temporary[i].scene_row][temporary[i].scene_column-1])
                        {
                            block[i] = true;
                            range--;
                        }
            }
        }
        if(temporary[i].scene_row == N-1)//sprawdzanie pozycji wertykalnej dolnej
        {
            if(temporary[i].scene_column == 0)//lewy dolny róg
            {
                if(map[temporary[i].scene_row-1][temporary[i].scene_column])
                    if(map[temporary[i].scene_row][temporary[i].scene_column+1])
                    {
                        block[i] = true;
                        range--;
                    }
            }
            else if(temporary[i].scene_column == N-1)//prawy dolny róg
            {
                if(map[temporary[i].scene_row-1][temporary[i].scene_column])
                    if(map[temporary[i].scene_row][temporary[i].scene_column-1])
                    {
                        block[i] = true;
                        range--;
                    }
            }
            else//brak skrajnej pozycji horyzontalnej
            {
                if(map[temporary[i].scene_row-1][temporary[i].scene_column])
                    if(map[temporary[i].scene_row][temporary[i].scene_column+1])
                        if(map[temporary[i].scene_row][temporary[i].scene_column-1])
                        {
                            block[i] = true;
                            range--;
                        }
            }
        }
        if(temporary[i].scene_column == 0 && temporary[i].scene_row != 0 && temporary[i].scene_row != N-1)//sprawdzanie pozycji
            //skrajnej horyzontalnej lewej kiedy brak skrajnej wertykalnej
        {
            if(map[temporary[i].scene_row-1][temporary[i].scene_column])
                if(map[temporary[i].scene_row][temporary[i].scene_column+1])
                    if(map[temporary[i].scene_row+1][temporary[i].scene_column])
                    {
                        block[i] = true;
                        range--;
                    }
        }
        if(temporary[i].scene_column == N-1 && temporary[i].scene_row != 0 && temporary[i].scene_row != N-1)//sprawdzanie pozycji
            //skrajnej horyzontalnej prawej kiedy brak skrajnej wertyklanej
        {
            if(map[temporary[i].scene_row-1][temporary[i].scene_column])
                if(map[temporary[i].scene_row+1][temporary[i].scene_column])
                    if(map[temporary[i].scene_row][temporary[i].scene_column-1])
                    {
                        block[i] = true;
                        range--;
                    }
        }
        if(temporary[i].scene_row > 0)
            if(temporary[i].scene_column < N-1)
                if(temporary[i].scene_row < N-1)
                    if(temporary[i].scene_column > 0)//sprawdzanie pozycji gdy nie ma skrajnych wertykalnych i horyzontalnych
                        if(map[temporary[i].scene_row-1][temporary[i].scene_column])
                            if(map[temporary[i].scene_row][temporary[i].scene_column+1])
                                if(map[temporary[i].scene_row+1][temporary[i].scene_column])
                                    if(map[temporary[i].scene_row][temporary[i].scene_column-1])
                                    {
                                        block[i] = true;//informacja o wykluczeniu sceny o numerze i
                                        range--;//zmniejszamy zakres z którego będzie wybierana scena
                                    }
    }
}
