#include "Player.h"

Player::Player(myScene *starting_scene)
{
    setRect(0, 0, 50, 50);
    setFlag(QGraphicsItem::ItemIsFocusable);
    set_curr_scene(starting_scene);
    curr_scene->addItem(this);
    setPos((curr_scene->width()/2)-(rect().width()/2), (curr_scene->height()/2)-(rect().height()/2));
    //niby działa, chociaż nie wiem jak bo teraz to wygląda tak, jakby te funkcje sie wykonywały za każdym razem gdy zmienia sie
    //scena i nie potrzeba samemu ich wywoływać tak jak próbowałem w myView::change_view(), nawet w następnych scenach obiekt player będzie
    //wchodizł w interkację z drzwiami ale tylko jeśli ich typ i liczba zgadza się z tymi które gracz miał na pierwszej scenie,
    //jeśli będzie ich mniej to gracz próbujący ich użyć przy pustej ścianie, zawiesi program, jeśli ich liczba będzie większa, to te nadmiarowe
    //nie będą działać lub wywalą program
}

void Player::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_W)
    {
        if(pos().y() >= area_begining_delimeter+10)
        {
            setPos(x(), y()-10);
        }
        else if(pos().y() > area_begining_delimeter)
        {
            setPos(x(), y()-1);
        }
    }
    else if(event->key() == Qt::Key_D)
    {
        if(pos().x()+rect().width() <= x_area_ending_delimeter-10)
        {
            setPos(x()+10, y());
        }
        else if(pos().x()+rect().width() < x_area_ending_delimeter)
        {
            setPos(x()+1, y());
        }
    }
    else if(event->key() == Qt::Key_S)
    {
        if(pos().y()+rect().height() <= y_area_ending_delimeter-10)
        {
            setPos(x(), y()+10);
        }
        else if(pos().y()+rect().height() < y_area_ending_delimeter)
        {
            setPos(x(), y()+1);
        }
    }
    else if(event->key() == Qt::Key_A)
    {
        if(pos().x() >= area_begining_delimeter+10)
        {
            setPos(x()-10, y());
        }
        else if(pos().x() > area_begining_delimeter)
        {
            setPos(x()-1, y());
        }
    }
    else if(event->key() == Qt::Key_E)
    {
        emit action();
    }
}

void Player::set_moving_area()
{
    if(curr_scene->get_floor())
    {
        area_begining_delimeter = curr_scene->get_wall_Size();
        x_area_ending_delimeter = curr_scene->width()-curr_scene->get_wall_Size();
        y_area_ending_delimeter = curr_scene->height()-curr_scene->get_wall_Size();
    }
    else if(!curr_scene->get_floor())
    {
        area_begining_delimeter = 0;
        x_area_ending_delimeter = curr_scene->width();
        y_area_ending_delimeter = curr_scene->height();
    }
}

void Player::set_curr_scene(myScene *scene_to_set)
{
    curr_scene = scene_to_set;

    if(curr_scene->get_door_up())
        connect(this, &Player::action, get_curr_scene()->get_door_up(), &Door::reaction);
    if(curr_scene->get_door_right())
        connect(this, &Player::action, get_curr_scene()->get_door_right(), &Door::reaction);
    if(curr_scene->get_door_down())
        connect(this, &Player::action, get_curr_scene()->get_door_down(), &Door::reaction);
    if(curr_scene->get_door_left())
        connect(this, &Player::action, get_curr_scene()->get_door_left(), &Door::reaction);

    set_moving_area();
    setFocus();

    //z tym są takie bugi że nie wiem czy nie warto wrócić do kolizji, nie zmieniając programu odpalam go 5 razy pod rząd i testuje w dokładnie
    //taki sam sposób a on raz działa a raz nie, jedyna szansa jaką dla tego jeszcze widzę dla discon i con, to robienie tego po wykonaniu
    //wszystkich odpowiedzialnych za zmianę sceny sygnałów i slotów np poprzez ustawienie flagi i dopisanie funkcji w player.h, ale raczej
    //nie powinna się ta funkcja wykonywać w trakcie change_view() tylko po nim, nyć może trzeba będzie użyć kolejnego connect żeby wywołać to

    //pierwszy pomysł: ustawić tutaj flagę informującą że scena się zmieniła i dokonać connect w keyPressEvent na samym początku by mieć pewność
    //że "zdąży" się połączyć przed próbą użycia

    //drugi pomysł: nie korzystać z tylu funkcji do sprawdzania czy gracz skorzystał z drzwi tylko, zrobić coś podobnego do door::reaction w
    //player::keyPressEvent w if'ie gdzie jest emit action()
    //curr_scene = scene_to_set;
}

myScene *Player::get_curr_scene(){return curr_scene;}
