#ifndef GENERATINGALGORITHM_H
#define GENERATINGALGORITHM_H

#include "myScene.h"

struct SceneSeries
{
    short scene_row, scene_column;
    myScene *created_scene;

    SceneSeries();
};

struct GenerateMap//gdyby SceneSeries z tego dziedziczyło to być może łatwiej by było generować korytarze skoro każda scena z serii mogłaby
        //wywołać tą funkcję, z drugiej strony, teraz nie trzeba tworzyć samemu zmiennych SceneSeries bo pierwsza zmienna może być tworzona w tym
        //konstruktorze, można tu też przechowywać takie zmienne jak pierwsza scena w serii lub ostatnia, albo najbardziej wysunięta w jakimś kierunku
{
    short max_corridors_in_map;
    short corridors_counter;//lub można zmienić na liczenie scen
    short max_scenes_in_corridor;// = 4; przy takich wartościach początkowych w pierwszej iteracji utworzy się 5 scen, 1 będzie początkową która zostanie
    short scenes_in_corridor_counter;// = 0; utworzona przed wywołaniej GenerateCorridor() a funkcja następnie
    //wygeneruje jeszcze 4, pierwsze 4 sceny licząc od początkowej
    short N;// = 9; zostaną zapisane w pamięci w celu losowania miejsca na wygenerowanie kolejnego korytarza
    bool **map;//mapa do określenia położenia scen (będzie tablicą)

    SceneSeries *head;
    SceneSeries *tail;
    SceneSeries *temporary;//jedna z tych 4 scen (temp będzie tablicą) będzie losowana po utworzeniu korytarza po to by losować kolejny,
    //powinna mieć rozmiar równy scenes_in_corridor

    GenerateMap();//może się przyda jeszcze kiedyś funkcja SetParamteres(); do ustawienia rzeczy typu N, scenes_in_corridor itp
    void StartGenerating();
    void GenerateCorridor(SceneSeries *);
    void determinate_possible_generation_range(short, bool *, short &);
};

#endif // GENERATINGALGORITHM_H
