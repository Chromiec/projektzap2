#ifndef MYSCENE_H
#define MYSCENE_H

#include <QGraphicsScene>
#include <QGraphicsRectItem>

#include "Door.h"

class myScene:
        public QGraphicsScene,
        public QGraphicsRectItem
{
    const unsigned short min_scene_Width, min_scene_Height;
    const unsigned short max_scene_Width, max_scene_Height;

    const unsigned short wall_Size;

    QGraphicsRectItem *floor;

    myScene *scene_up;
    myScene *scene_right;
    myScene *scene_down;
    myScene *scene_left;

    Door *door_up;
    Door *door_right;
    Door *door_down;
    Door *door_left;

public:
    myScene(unsigned short = 800, unsigned short = 600);

    void set_mySceneRect(unsigned short, unsigned short);
    void Floor(bool);//ustawianie czy podłoga ma być (!0) czy ma jej nie być (0)

    void set_scene_up(myScene *);
    void set_scene_right(myScene *);
    void set_scene_down(myScene *);
    void set_scene_left(myScene *);

    unsigned short get_wall_Size();//player.cpp, set_moving_area()
    QGraphicsRectItem *get_floor();//player.cpp, set_moving_area()
    myScene *get_scene_up();//door.cpp, emit door_used(), w reaction()
    myScene *get_scene_right();
    myScene *get_scene_down();
    myScene *get_scene_left();
    Door *get_door_up();
    Door *get_door_right();
    Door *get_door_down();
    Door *get_door_left();
};

#endif // MYSCENE_H
