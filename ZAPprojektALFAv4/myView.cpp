#include "myView.h"

#include "Player.h"

extern Player *player;

myView::myView()
{
    show();//to chyba wyciągne później do klasy Game
    //tu będzie setFixedSize() w zależności od tego jak ustalimy co z rozdzielczością
    setFixedSize(800, 600);//tymczasowo
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void myView::change_view(unsigned short door_id)
{
    if(player->get_curr_scene()->get_door_up())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_up(), &Door::reaction);
    if(player->get_curr_scene()->get_door_right())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_right(), &Door::reaction);
    if(player->get_curr_scene()->get_door_down())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_down(), &Door::reaction);
    if(player->get_curr_scene()->get_door_left())
        disconnect(player, &Player::action, player->get_curr_scene()->get_door_left(), &Door::reaction);

    scene()->removeItem(player);
    switch(door_id)
    {
        case 0:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_up());
            player->setPos((player->get_curr_scene()->width()/2)-(player->rect().width()/2),
                           (player->get_curr_scene()->height())-(player->rect().height())-(100+30));
        }
            break;
        case 1:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_right());
            player->setPos(100+30,
                           (player->get_curr_scene()->height()/2)-(player->rect().height()/2));
        }
            break;
        case 2:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_down());
            player->setPos((player->get_curr_scene()->width()/2)-(player->rect().width()/2),
                           100+30);
        }
            break;
        case 3:
        {
            player->set_curr_scene(player->get_curr_scene()->get_scene_left());
            player->setPos((player->get_curr_scene()->width())-(player->rect().width())-(100+30),
                           (player->get_curr_scene()->height()/2)-(player->rect().height()/2));
        }
            break;
    }

    setScene(player->get_curr_scene());
    scene()->addItem(player);
}
